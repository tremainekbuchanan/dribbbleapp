dribbbleapp
===========

Simple dribbble app showing how many likes a user has on the site.

Improvement to come:

1. Validation of user input
2. Appropriate error messages if user is not found on dribbble.com
3. Pull teaser images and basic dribbble info on players
