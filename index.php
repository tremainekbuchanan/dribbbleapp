<!DOCTYPE html>
<?php include 'header.php' ?>

  <body>
      
      
    <div class="container">
      <div class="page-header">
        <h3>Are you well liked on Dribbble.com</h3>
       
        </div>
        <div class="well" id="main">
        
            <form role="form">
            
            <div class="form-group">
             <label for="txtuname" >Dribbble Username</label>   
             <input type="text" class="form-control" placeholder="Enter Dribbble Username" id="txtuname" autocomplete="off">
               
                <p class="help-block">Example:simplebits</p>
                <button type="button" class= "btn btn-info" name="getlikes" id="btnget">Get Likes <span class="glyphicon glyphicon-thumbs-up"></span></button>
            </div>
            </form>
            
        </div>
        
        <div class="well">
        <p>This application basically uses the Dribbble API and gets the number of likes that a user has.</p>
        </div>

      
        
        <div class="footer">
        <p>&copy; Site Developed by Byte Solutions 2013</p>
      </div> 

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
      
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
    <script src="scripts.js"></script>
  </body>
</html>